const express = require("express");
const router = express.Router();


const userController = require("../controllers/userController");
const auth = require("../auth");

router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});



//[Min.Req] User registration

router.post("/register", (req, res) => {
	userController.registerNewUser(req.body).then(resultFromController => 
		res.send(resultFromController));
});


//[Min.Req] User authentication

router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})



//[Min.Req] Non-admin User checkout (Create Order)

router.post("/createOrder",  (req, res) => {
	const orderData = {
		
		userId : auth.decode(req.headers.authorization).id,
		productId : req.body.productId,
		quantity:req.body.quantity
	}
	userController.createUserOrder(orderData).then(resultFromController => res.send(resultFromController));
});



/*router.post("/createOrder", auth.verify, (req, res) => {
	const orderData = {
		isAdmin : auth.decode(req.headers.authorization).isAdmin,
		userId : auth.decode(req.headers.authorization).id,
		productId : req.body.productId,
		quantity:req.body.quantity
	}
	userController.createUserOrder(orderData).then(resultFromController => res.send(resultFromController));
});
*/


//[Min.Req] Retrieve User Details

router.get("/details" , auth.verify, (req, res) => {
	const userDetails = auth.decode(req.headers.authorization)
	userController.getProfile({userId : userDetails.id}).then(resultFromController => res.send(resultFromController));
});













// [Stretch Goal] Set user as admin (Admin Only)
router.patch("/:userId/status", auth.verify, (req,res) => { 
	const statusChange = {
		user : req.body,
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}
	userController.changeUserStatus(req.params, statusChange).then(resultFromController => res.send(resultFromController));
})

// [Stretch Goal] Retrieve authenticated user’s orders

router.get("/allorders", auth.verify, (req, res) => {
	const isUser = {
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}
	userController.viewAllOrder(isUser).then(resultFromController => res.send(resultFromController));
});




module.exports = router;
