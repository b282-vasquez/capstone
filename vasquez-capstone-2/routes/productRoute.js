const express = require("express");
const router = express.Router();

const productController = require("../controllers/productController")

const auth = require("../auth");
//
router.post("/checkProduct" , (req, res) => {
	productController.checkProductExists(req.body).then(resultFromController => res.send(resultFromController))
});

//[Min.Req] Create product(Admin Only) 

router.post("/addProduct",  (req, res) => {
	const data = {
		product : req.body,
		//isAdmin : auth.decode(req.headers.authorization).isAdmin
	}
	productController.addProduct(data).then(resultFromController => res.send(resultFromController));
});



//[Min.Req] Retrieve all products

router.get("/allProduct", (req, res) => {
	
	productController.allProduct().then(resultFromController => res.send(resultFromController));
});


//[Min.Req] Retrieve all active products(User view)

router.get("/availableProduct", (req, res) => {
	
	productController.activeProduct().then(resultFromController => res.send(resultFromController));
});


/*router.get("/availableProduct", auth.verify, (req, res) => {
	const activeData = {
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}
	productController.activeProduct(activeData).then(resultFromController => res.send(resultFromController));
});
*/

//[Min.Req] Retrieve single product

router.get("/:productId",  (req,res) => {
	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
});


//[Min.Req] Update Product information (Admin only)

router.put("/:productId", (req, res) => {
	const adminData = {
		product : req.body
	}
	productController.updateProduct(req.params, adminData).then(resultFromController => res.send(resultFromController));
});



//[Min.Req] Archive Product (Admin only)

router.patch("/:productId",  (req, res) => {
	const archiveProduct = {
		product: req.body,
		
	}
	productController.archiveProduct(req.params, archiveProduct).then(resultFromController => res.send(resultFromController));
});


//[Min.Req] Active Product (Admin only)

router.patch("/:productId/activate", (req, res) => {
	const activateProduct = {
		product: req.body
		
	}
	productController.activateProduct(req.params, activateProduct).then(resultFromController => res.send(resultFromController));
});





module.exports = router;