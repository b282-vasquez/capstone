const jwt = require("jsonwebtoken");

const secret = "I-am-a-Web-Developer!";

// Token Creation Section

module.exports.createAccessToken = (user) =>{
	const data = {
		id : user._id,
		email : user.email,
		isAdmin : user.isAdmin,
		productId : user.productId

		
	};
	return jwt.sign(data, secret, {});
};

//Sample Code for Order

module.exports.orderProduct = (order) => {
	const orderData = {
		id : user._id,
		productId : order.productId,
		quantity : order.quantity
	}
}

//Token Verification Section

module.exports.verify = (req, res, next) => {
	let token = req.headers.authorization;
	if(typeof token !== "undefined"){
		token = token.slice(7, token.length);
	return jwt.verify(token, secret, (err, data) => {
		if(err) {
			return res.send({auth : "failed"});
		} else {
			next();
		}
	})
	} else {
		return res.send({auth : "failed"});
	};
};

// Token Decryption Section

module.exports.decode =  (token) => {
	if (typeof token !== "undefined"){
		token = token.slice(7, token.length);
		return jwt.verify(token, secret, (err, data) => {
			if(err) {
				return null;
			} else {
				return jwt.decode(token, {complete: true}).payload;
			}
		})
	} else {
		return null;
	};
};

