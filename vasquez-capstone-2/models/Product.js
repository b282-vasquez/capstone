const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
	name : {
		type : String,
		required : [true, "Product's name is required."]
	},
	description : {
		type : String,
		required : [true, "Product's description is required."]
	},
	price : {
		type : Number,
		required : [true, "Product's price is required."]
	},
	isActive : {
		type : Boolean,
		default : true
	},
	createdOn : {
		type : Date,
		default : new Date()
	},
	userOrders : [
		{
			userId : {
				type: mongoose.Schema.Types.ObjectId,
				ref: "User"
			},
			orderId: {type:String}
		}
	]
});

module.exports = mongoose.model("Product", productSchema);