const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	email : {
		type : String,
		required : [true, "Username is required."]
	},
	password : {
		type : String,
		required : [true, "Password is required."]
	},
	isAdmin :{
		type : Boolean,
		default : false
	},
	orderProduct : [
		{
			products : [
				{
					productId : {
						type: mongoose.Schema.Types.ObjectId,
						ref: 'Product'
					},
					productName: String,
					quantity : {
						type : Number,
					//	required : [true, "Quantity is required."]
					}
				}
			],
			totalAmount : {
				type : Number
			},
			purchasedOn : {
				type : Date,
				default : new Date()
			}
		}
	]
});

module.exports = mongoose.model("User", userSchema);