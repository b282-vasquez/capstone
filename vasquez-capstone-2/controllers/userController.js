const User = require("../models/User");
const Product = require("../models/Product");

const bcrypt = require("bcrypt");
const auth = require("../auth");

module.exports.checkEmailExists = (reqBody) => {
	// The result is sent back to the Postman via the "then" method found in the route file
	return User.find({email : reqBody.email}).then(result => {
		// The "find" method returns a record if a match is found
		if (result.length > 0) {
			return true;
		// No duplicate email found
		// The user is not yet registered in the database
		} else {
			return false;
		};
	});
};

//[Min.Req] User registration
module.exports.registerNewUser = (reqBody) => {
	return User.find({email : reqBody.email}).then(result => {

		/*if(result.length > 0) {
			return `"${reqBody.email}" is already exist, please try another email`
		}*/

	let newUser = new User({
		email : reqBody.email,
		password : bcrypt.hashSync(reqBody.password, 10)
	});

	return newUser.save().then((user, error) =>{
		if(error){
			return false;
		} else {
			return true;
		} 
	});
});
};


//[Min.Req] User authentication
module.exports.loginUser = (reqBody) => {
	return User.findOne({email : reqBody.email}).then(result => {
		if (result == null){
			return false
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			if(isPasswordCorrect) {
				return{access : auth.createAccessToken(result)}
			} else {
				return false;
			};
		};
	});
};


//[Min.Req] Non-admin User checkout (Create Order)

module.exports.createUserOrder = async(data) => {
	// if(data.isAdmin === false){
			try{
				let user = await User.findById(data.userId)

				let product = await Product.findById(data.productId)
				let quantity = data.quantity
				let price = product.price
				let totalAmount = price * quantity

				user.orderProduct.push({
					products: [{
						productId: data.productId,
						productName: product.name,
						quantity: quantity
					}],
					totalAmount: totalAmount,
					purchasedOn: new Date()
				})
				await user.save()

				product.userOrders.push({userId: data.userId, orderId : data.productId})
				await product.save()
				return true
			} catch (error) {
				console.log(error)
				return false
			}
		// } else {
		// 	return "This features is for users only"
		// }
}





//[Min.Req] Retrieve User Details

module.exports.getProfile = (data) =>{
	return User.findById(data.userId)
	.then(result => {
		result.password = ""
		return result;
	});
};









// [Stretch Goal] Set user as admin (Admin Only)
module.exports.changeUserStatus = (reqParams, statusChange) => {
	if(statusChange.isAdmin){
		let userStatus = {
			isAdmin : true
		};
		return User.findByIdAndUpdate(reqParams.userId, statusChange).then((status, err) => {
			if(err){
				return false;
			} else {
				return "User status changed Successfully."
			};
		});
	};
	let message = Promise.resolve("User must be a admin to access this.")
	return message.then((value) => {
		return{value}
	});
};

// [Stretch Goal] Retrieve authenticated user’s orders


module.exports.viewAllOrder = (allOrders) => {
	if(allOrders.isAdmin == false){
		return User.find({isAdmin: false})

		.then(result => {
			return result;
		})
	} else {
		return User.find({})
		.select('_id email orderProduct productId productName quantity')
		.then(result => {
			return result;
		})
	}
}