const Product = require("../models/Product");

//
module.exports.checkProductExists = (reqBody) => {
	return Product.find({name: reqBody.name}).then(result=> {
		if (result.length > 0) {
			return true
		} else {
			return false
		}
	})
}


//[Min.Req] Create product(Admin Only) 

module.exports.addProduct = (data) => {
	return Product.find({name: data.product.name}).then( result => {
		
	
	
		let newProduct = new Product ({
			name : data.product.name,
			description : data.product.description,
			price : data.product.price
		});
		return newProduct.save().then((product, error) => {
			if(error) {
				return false;
			} else {
				return true;
			};
		});
	
	
  });
};


//[Min.Req] Retrieve all products (Admin only)

module.exports.allProduct = () => {
	// if(allData.isAdmin){
		return Product.find({})
		//.select('_id name description price isActive')
		.then(result => {
			return result;
		});
	// }
	// let message = Promise.resolve("User must be an Admin to access this.")
	// return message.then((value) => {
	// 	return {value}
	// });
};


//[Min.Req] Retrieve all active products(User view)

module.exports.activeProduct = (activeData) => {
	// if(activeData.isAdmin == false){
		return Product.find({isActive: true})
		// .select('name description price')
		.then(result => {
			return result;
		});
	// } else {
	// 	return Product.find({}).then(result => {
	// 		return result;
	// 	})
	// }

};

//[Min.Req] Retrieve single product
module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams.productId)
	.select('_id name description price isActive')
	.then(result => {
		return result;
	});
};


//[Min.Req] Update Product information (Admin only)

module.exports.updateProduct = (reqParams,adminData) => {
	//if(adminData.isAdmin){
		let updatedProduct = {
			name : adminData.product.name,
			description : adminData.product.description,
			price : adminData.product.price
		};
		return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {
			if(error){
				return false;
			} else {
				return product;
			};
		});
	//}
	let message = Promise.resolve("User must be an Admin to access this.")
	return message.then((value) => {
		return {value}
	});
};


//[Min.Req] Archive Product (Admin only)

module.exports.archiveProduct = (reqParams, archiveProduct) => {

		let archivedProduct = {
			isActive: false
		};
		return Product.findByIdAndUpdate(reqParams.productId, archivedProduct).then((course, err) => {
			if(err){
				return false;
			} else {
				return course;
			};
		});
	
	let message = Promise.resolve("User must be Admin to access this.")
	return message.then((value) => {
		return{value}
	});
};


//[Min.Req] Active Product (Admin only)

module.exports.activateProduct = (reqParams, activateProduct) => {
	
		let activateProducts = {
			isActive: true
		};
		return Product.findByIdAndUpdate(reqParams.productId, activateProducts).then((course, err) => {
			if(err){
				return false;
			} else {
				return course;
			};
		});
	
	let message = Promise.resolve("User must be Admin to accrss this.")
	return message.then((value) => {
		return{value}
	});
};

