const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const userRoute = require("./routes/userRoute");
const productRoute = require("./routes/productRoute");
// const orderRoute = require("./routes/orderRoute");
// const cartRoute = require("./routes/cartRoute");

const app = express();

// MongoDB Atlas Connection
mongoose.connect("mongodb+srv://mrvasquezr:o1FY87gxoTRobrX5@wdc028-course-booking.edafbzc.mongodb.net/eCommerce-API-Capstone2", 
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

// MongoDB Local connection
mongoose.connection.once("open", () => console.log("Im so proud of you! We are now successfully connected to the cloud database! YAHOO!!!"));

//Middleware
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors());

app.use("/users", userRoute);
app.use("/products", productRoute);
// app.use("/orders", orderRoute);
// app.use("/userCart", cartRoute);

app.listen(process.env.PORT || 8888, () => console.log(`Relax champ ^_^ , I am now listening to port ${process.env.PORT || 8888}`));