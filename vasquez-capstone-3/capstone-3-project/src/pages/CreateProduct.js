
	import {useState, useEffect, useContext} from 'react';

	import UserContext from '../UserContext';
	import {Navigate, useNavigate} from 'react-router-dom';
	import Swal from 'sweetalert2';
	import {Form, Button} from 'react-bootstrap';
	import {Link} from 'react-router-dom';


	export default function CreateProduct() {
		const {user} = useContext(UserContext);

		const navigate = useNavigate();

		const [name, setName] = useState("");
		const [description, setDescription] = useState("");
		const [price, setPrice] = useState("");
		const [isActive, setIsActive] = useState(false);

		useEffect(()=> {
			if(name !== "" && description !== "" && price !== "") {
				setIsActive(true)
			} else {
				setIsActive(false)
			}
		},[name, description, price]);

		function addProduct(e) {
			e.preventDefault();

			fetch(`${process.env.REACT_APP_API_URL}/products/checkProduct`, {
				method: "POST",
				headers: {
					'Content-Type' : 'application/json'
				},
				body: JSON.stringify({
					name: name
				})
			})
			.then(res => res.json())
			.then(data => {
				console.log(data)
				if (data) {
					Swal.fire({
					    title: "Product name already exist",
					    icon: "error"
					})
				} else {
					fetch(`${process.env.REACT_APP_API_URL}/products/addProduct`, {
						method: "POST",
						headers: {
							'Content-Type' : 'application/json'
						},
						body: JSON.stringify({
							name: name,
							description: description,
							price: price
						})
					})
					.then(res => res.json())
					.then(data => {
						console.log(data);

						if (data) {
							setName("");
							setDescription("");
							setPrice("");

							Swal.fire({
							    title: "Successful",
							    icon: "success",
							    text: "New product added!"
							})
							navigate("/dashboard");
						} else {
							Swal.fire({
	                            title: "Something went wrong",
	                            icon: "error",
	                            text: "Please, try again."
	                        })
						} //LINE 73
					}) //LINE 59
				} // LINE 46
			}) // LINE 39
		} // LINE 26

		return (
		<>
		{
			(user.isAdmin) ?
			<Navigate to = "/createProduct" /> 
			:
			<Navigate to = "/" />

		}
		    <Form onSubmit={(e) => addProduct(e)} >
		

		              <h1 className="text-center my-3">New Product</h1>

		              <Form.Group className="mb-3" controlId="firstName">
		                <Form.Label>Product Name</Form.Label>
		                <Form.Control 
		                    type="text"
		                    value={name}
		                    onChange={(e) => {setName(e.target.value)}}
		                    placeholder="Product name" 
		                    required
		                    />
		              </Form.Group>

		              <Form.Group className="mb-3" controlId="lastName">
		                <Form.Label>Product Description</Form.Label>
		                <Form.Control 
		                    type="text"
		                    value={description}
		                    onChange={(e) => {setDescription(e.target.value)}}
		                    placeholder="Product description" />
		              </Form.Group>

		              <Form.Group className="mb-3" controlId="userEmail">
		                <Form.Label>Product Price</Form.Label>
		                <Form.Control 
		                    type="number"
		                    value={price}
		                    onChange={(e) => {setPrice(e.target.value)}}
		                    placeholder="0" />
		                
		              </Form.Group>

		              
		              { isActive ?
		              			<>
		                        <Button variant="primary" type="submit" id="submitBtn">
		                         Submit
		                        </Button>
		                        <Button variant="danger" size = "sm" style ={{width:'auto'}} as = {Link} to={'/dashboard'} >Back to Dashboard</Button>
		                        </>
		                        :
		                        <>
		                        <Button variant="primary" type="submit" id="submitBtn" disabled>
		                          Submit
		                        </Button> {' '}
		                        <Button variant="danger" size = "sm" style ={{width:'auto'}} as = {Link} to={'/dashboard'} >Back to Dashboard</Button>
		                        </>
		              }
		   		             
		    </Form> 
		   </>
		)

	}


