import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';


import {Link} from 'react-router-dom';
import UserContext from '../UserContext'; 
import {useContext} from 'react';
import {Navigate} from 'react-router-dom'; 


function Dashboard() {
  const {user} = useContext(UserContext);

  return (
    <Card className="text-center">
      <Card.Header>Admin Dashboard</Card.Header>
       {
        (user.isAdmin) 
        ?
        <Card.Body>
        <Card.Title>Welcome, Admin!</Card.Title>
        <Card.Text>
          
        </Card.Text>
        <Button variant="primary" size = "lg" style ={{width:'auto'}} as = {Link} to={'/createProduct'} >Create Product</Button>{' '} 
        <Button variant="success" size = "lg" style ={{width:'auto'}} as = {Link} to ={'/viewProducts'}>View all products</Button>
      </Card.Body>
      :
      <Navigate to = "/" />

       }
      
      <Card.Footer className="text-muted"></Card.Footer>
    </Card>
  );
}

export default Dashboard;


