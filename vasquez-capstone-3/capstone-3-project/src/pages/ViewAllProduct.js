
  import {useContext} from 'react';
  import AllProduct from '../components/AllProduct';
  import Button from 'react-bootstrap/Button';
  import {Link, Navigate} from 'react-router-dom';
  import UserContext from '../UserContext'; 





import {useState, useEffect} from 'react';

  export default function ViewAllProducts() {
    const [products, setProducts] = useState([]);
    const {user} = useContext(UserContext);

    useEffect(()=> {
      fetch(`${process.env.REACT_APP_API_URL}/products/allProduct`)
      .then(res => res.json())
      .then(data=> {
        console.log(data)
        setProducts(data.map(product => {
          console.log(product)
          return (
            
            <AllProduct key = {product.id} product = {product} />
            )
        }))
      })
    }, [])

    return (

      <>
      {
        (user.isAdmin) ? 
        <Navigate to = "/viewProducts" />
        :
        <Navigate to = "/" />

      }
        <h1>Welcome Admin</h1>
        <Button variant="primary" size = "sm" style ={{width:'auto'}} as = {Link} to={'/dashboard'} >Back to Dashboard</Button>
        {products}
      </>
      )
};

