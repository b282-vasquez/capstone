import Banner from '../components/Banner';
import Highlights from '../components/Highlights';


export default function Home() {

	const data = {
		title: "Diswashing Detergent Needs",
		content: "Be Spotless, Be Bright",
		destination: "/products",
		label: "Order now!"
	}


	return (
		<>
		<Banner data={data} />
    	<Highlights />
    	
		</>
	)
}
