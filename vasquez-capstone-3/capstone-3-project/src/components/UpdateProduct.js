
import {useState, useEffect} from 'react';
import { Button } from 'react-bootstrap';
import {useParams, Link, useNavigate} from 'react-router-dom';

import Swal from 'sweetalert2';


const UpdateProduct = () => {
	const [name, setName] = useState('')
	const [description, setDescription] = useState('')
	const [price, setPrice] = useState('')
	const [isActive] = useState('');
	const params = useParams();
	const {productId} = useParams();
	const navigate = useNavigate();
	

	/*useEffect(() => {
		getProductDetails();
	},[])

	const getProductDetails = async () => {
		console.log(params)
		let result = await fetch(`${process.env.REACT_APP_API_URL}/products/${params.productId}`);
		result = await result.json();
		console.log(result)
		setName(result.name);
		setDescription(result.description);
		setPrice(result.price);
	}
*/
	useEffect(() => {
	    const getProductDetails = async () => {
	      console.log(params);
	      let result = await fetch(
	        `${process.env.REACT_APP_API_URL}/products/${params.productId}`
	      );
	      result = await result.json();
	      console.log(result);
	      setName(result.name);
	      setDescription(result.description);
	      setPrice(result.price);
	    };

	    getProductDetails();
	  }, [params]); 


	const updateProduct = async() => {
		console.log(name,description,price)
		let result = await fetch(`${process.env.REACT_APP_API_URL}/products/${params.productId}`, {
			method: 'PUT',
			body: JSON.stringify({name,description,price}),
			headers: {
				'Content-Type': 'application/json'
			}
		});
		 result = await result.json()
		 console.log(result);
		 Swal.fire({
                            title: "Product update Successful",
                            icon: "success",
                            text: "Welcome!!!"
                        })

		 navigate('/viewProducts') 

	}

		const setStatusDeactivate = async() => {
			console.log(name, description, price, isActive)
			const newStatusDeact = !isActive

			let result = await fetch(`${process.env.REACT_APP_API_URL}/products/${params.productId}`, {
				method: 'PATCH',
				body: JSON.stringify({
					isActive: newStatusDeact
				}),
				headers: {
					'Content-Type' : 'application/json'
				}
			});
			result = await result.json()
			console.log(result);
			Swal.fire({
	                            title: "Product Deactivation Successful",
	                            icon: "success",
	                            text: "You successfully update this product status"
	                        })
			navigate("/viewProducts")
			}

		const setStatusActivate = async() => {

			console.log(name, description, price, isActive)
			const newStatusAct = isActive
			let result = await fetch(`${process.env.REACT_APP_API_URL}/products/${params.productId}/activate`, {
				method: 'PATCH',
				body: JSON.stringify({
					isActive: newStatusAct
				}),
				headers: {
					'Content-Type' : 'application/json'
				}
			});
			result = await result.json()
			console.log(result);
			Swal.fire({
	                            title: "Product Activation Successful",
	                            icon: "success",
	                            text: "You successfully update this product status"
	                        })
			navigate("/viewProducts")

		}

	return (
		<div className = 'update-product'>
			<h1>Update Product</h1>
			
			<input className = 'inputBox' type = 'text' placeholder = 'Enter product name' value = {name} onChange = {(e) => { setName (e.target.value)}}/>
			
			<input className = 'inputBox' type = 'text' placeholder = 'Enter product name' value = {description} onChange = {(e) => { setDescription (e.target.value)}}/>
			
			<input className = 'inputBox' type = 'number' placeholder = 'Enter product name' value = {price} onChange = {(e) => { setPrice (e.target.value)}}/>
			
			
			<Button className = 'appButton' variant="primary" onClick={() => updateProduct(productId)} >Update</Button>{' '}

			<Button className = 'appButton' variant="danger" onClick={() => setStatusDeactivate(productId)} >Deactivate</Button>{' '}

			<Button className = 'appButton' variant="success" onClick={() => setStatusActivate(productId)} >Activate</Button>{' '}

			<Button className = 'appButton' variant="warning" as = {Link} to = "/viewProducts" >Cancel</Button>
			 
			
		</div>
		)
}
export default UpdateProduct;