import {useState, useContext, useEffect} from 'react';
import {Container, Button, Row } from 'react-bootstrap';
import {useParams, Link, useNavigate} from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function ProductView() {
	const {user} = useContext(UserContext);
	const navigate = useNavigate();

	const [quantity, setQuantity] = useState(1);
	const {productId} = useParams();
	const [name, setName] = useState("")
	const [description, setDescription] = useState("")
	const [price, setPrice] = useState(0);

	const order = (productId) => {
		fetch(`${process.env.REACT_APP_API_URL}/users/createOrder`, {
			method: "POST",
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productId:productId,
				quantity:quantity
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			if(data) {
				Swal.fire({
						title: "Thank you so much!",
						icon: "success",
						text: "You have successfully create an order."
					})
				navigate("/products")
			} else {
				Swal.fire({
						title: "Something went wrong",
						icon: "error",
						text: "Please try again."
					})
			}
		})
	};
	useEffect(() => {
		console.log(productId);

		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data);
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		})
	}, [productId]);
	
	const inputStyle = {
    textAlign: 'center', // Sets the text alignment to center

  	};


	return (

		<Container>
			
		<Row>
		<br/>
			<div className = 'update-product' style={{width: '50%', margin:'auto'}}>

				<h4 style={{color:'blue'}}>{name}</h4> <hr/> <br/>
				{
					(user.id) ?
					
				
				<div style={{paddingLeft: "1.5rem"}}>
				<p>{description}</p>
				<h5 >price: {price}</h5>

				<input style={inputStyle} type = 'number'  placeholder = 'Enter Quantity' value = {quantity} onChange = {(e) => { setQuantity (e.target.value)}}/>

				</div>
				:
				<div style={{paddingLeft: "1.5rem"}}>
				<p>{description}</p>
				<h5 >price: {price}</h5>
				
				</div>


				}

				{
					(user.id !== null) ?
					<div >
					    <Button style={{width:'25%'}} className = 'appButton' variant="primary" onClick={() => order(productId)} >Place an Order</Button>
					    <Button style={{width:'25%'}} className = 'appButton' variant="danger" as = {Link} to={'/products'} >Cancel</Button>
					</div>

					:

					<div >
					    <Button style={{width:'25%'}} className = 'appButton' variant="primary" as = {Link} to = "/login" >Please Login</Button>
					    <Button style={{width:'25%'}} className = 'appButton' variant="danger" as = {Link} to={'/products'} >Cancel</Button>
					</div>
				}

				     	
				
			</div>
		</Row>
		</Container>

	)
}// LINE 7