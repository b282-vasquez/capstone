import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
// import NavDropdown from 'react-bootstrap/NavDropdown';

import {useContext} from 'react';
import { Link} from 'react-router-dom';

import UserContext from '../UserContext';


function AppNavbar() {

  const {user} = useContext(UserContext);
  console.log(user);


  return (
    <Navbar expand="lg" className="bg-body-tertiary">
      <Container>
        <Navbar.Brand as = {Link} to = "/">DDNeeds</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto">
           
            
            {
              
              (user.isAdmin) ?

              <>   
              <Nav.Link as = {Link} to = "/dashboard">Dasboard</Nav.Link>            
              <Nav.Link as = {Link} to = "/logout">Logout</Nav.Link> 

              </>
                
              :

              (
                (user.id) ?
                            <>
                              <Nav.Link as = {Link} to = "/">Home</Nav.Link>
                              <Nav.Link as = {Link} to = "/products">Products</Nav.Link>
                              <Nav.Link as = {Link} to = "/logout">Logout</Nav.Link> 
                            </> 
              
                            
                            :
              
                            <>
                              <Nav.Link as = {Link} to = "/">Home</Nav.Link>
                              <Nav.Link as = {Link} to = "/products">Products</Nav.Link>
                              <Nav.Link as = {Link} to = "/login">Login</Nav.Link>
                              <Nav.Link as = {Link} to = "/register">Register</Nav.Link>  
              
                            </> 
              )
            }

          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}

export default AppNavbar;