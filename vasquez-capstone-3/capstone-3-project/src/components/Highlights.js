// import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';

function Highlights() {
  return (

    <Card className="text-center">
      <Card.Header>Welcome to my shop!</Card.Header>
      <Card.Body>
        <Card.Title><em>Make dishwashing a pleasure, not a chore.</em></Card.Title> <br/>
        <Card.Text>
        <h3>Our dishwashing liquid fights grime and grease so you don’t have to.</h3>
        
        </Card.Text>
       
      </Card.Body>
      <Card.Footer className="text-muted">Enjoy Shopping!</Card.Footer>
    </Card>
  );
}

export default Highlights;