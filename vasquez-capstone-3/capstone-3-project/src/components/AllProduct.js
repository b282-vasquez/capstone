import {Card, Container,Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';

function AllProduct({product}) {
  const {name, description, price, _id, isActive} = product;
  console.log(isActive)
  return (

    <Container fluid>
    <br/>
    {
    (isActive) ?
      <Card style={{ width: 'auto', backgroundColor: 'lightGreen', }}>
        <Card.Body>
          <Card.Title>{name}</Card.Title>
          <Card.Subtitle className="mb-2 text-muted" >{_id}</Card.Subtitle>
          <Card.Text>
            <hr/>
            {description} 
            <hr/>
            <strong> Price: {price}</strong>
          </Card.Text>

          <Button variant="primary" type="submit" id="submitBtn" size="sm" as = {Link} to ={`/products/update/${_id}`}>
              Update Product
          </Button>

        </Card.Body>
      </Card>
      :
      <Card style={{ width: 'auto' }}>
      <h5 style = {{color:'red', margin:'auto', paddingTop: '1rem'}}><strong>Product Deactivated</strong></h5><hr/>
        <Card.Body>

          <Card.Title><s>{name}</s></Card.Title>
          <Card.Subtitle className="mb-2 text-muted" >{_id}</Card.Subtitle>
          <Card.Text>
            <s>{description}</s> 
            <br/> <strong><s> Price: {price}</s></strong>
          </Card.Text>

          <Button variant="primary" type="submit" id="submitBtn" size="sm" as = {Link} to ={`/products/update/${_id}`}>
              Update Prodcut
          </Button>
        </Card.Body>
      </Card>
    }
     </Container> 
    );
}

export default AllProduct;
