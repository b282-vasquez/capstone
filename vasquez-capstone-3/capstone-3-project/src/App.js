  import {useState, useEffect} from 'react';
  import Login from './pages/Login';
  import Logout from './pages/Logout';
  import Register from './pages/Register';
  import Home from './pages/Home';


  import Appnavbar from './components/AppNavbar';
  import ProductView from './components/ProductView';
  import UpdateProduct from './components/UpdateProduct';
 


  import Products from './pages/Products'
  import Dashboard from './pages/Dashboard'
  import Error from './pages/Error';
  import CreateProduct from './pages/CreateProduct';
  import ViewAllProduct from './pages/ViewAllProduct';
 

  import './App.css';

  import {Container} from 'react-bootstrap';
  import {UserProvider} from './UserContext';
  import {BrowserRouter as Router, Routes, Route} from 'react-router-dom';


  function App() {

    const [user, setUser] = useState({
      id: null,
      isAdmin: null
    });

    const unsetUser = () => {
      localStorage.clear();
    };

    useEffect(() => {
      fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      })
      .then(res => res.json())
      .then(data => {
        if(typeof data._id !== "undefined") {
          setUser({
            id: data._id,
            isAdmin: data.isAdmin
          })
        } else {
          setUser({
            id: null,
            isAdmin: null
          })
        }
      }) // CLOSING TAG of LINE 29
    }, []) // CLOSING TAG of LINE 22

    return (
      <>
        <UserProvider value = {{user, setUser, unsetUser}}>
          <Router>
              <Appnavbar />
              <Container>
                <Routes>
                  <Route path = "/" element = {<Home />} />
                  <Route path = "/products" element = {<Products />} />
                  <Route path = "/products/:productId" element = {<ProductView />} />
                  <Route path = "/products/update/:productId" element = {<UpdateProduct />} />
                
        
                  <Route path = "/login" element = {<Login />} />
                  <Route path = "/logout" element = {<Logout />} />              
                  <Route path = "/register" element = {<Register />} />
                  <Route path = "/dashboard" element = {<Dashboard />} />
                  <Route path = "/createProduct" element = {<CreateProduct />} />
                  <Route path = "/viewProducts" element = {<ViewAllProduct />} />

                   

                  <Route path = "/*" element = {<Error />} />
                </Routes>
              </Container>
          </Router>
        </UserProvider>

      </>
    );
  }

  export default App;


